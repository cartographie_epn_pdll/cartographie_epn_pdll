class LieuMediation {
  float lat, lon, implicationPN, taillePicto; 
  int inscritPN;
  String nom, lieu, partenariat, label, implicationString;
  Location ou;
  color PDLL, PN, CYBERBASE, POINTCYB, ACCOORD, EMILA, CSC, RENCONTRES, IMMERSIONS, CYBANJOU;

  LieuMediation(float _lat, float _lon, int _inscritPN, String _nom, String _lieu, String _partenariat, String _label, String _implicationString) {
    lat = _lat;
    lon = _lon;

    implicationString = _implicationString;
    if (implicationString.equals("1,5")) implicationString = "1.5";
    implicationPN = Float.valueOf(implicationString);

    inscritPN = _inscritPN;
    nom = _nom;
    lieu = _lieu;
    partenariat = _partenariat;
    label = _label;
    println(implicationPN);
    // permet de visualiser le taux d'implication des EPN dans PN
    if (implicationPN != 0) { 

      taillePicto = 20f * implicationPN;
      //println(taillePicto+"    "+implicationPN);
    }
    else {
      taillePicto = 20;
    }
    ou = new Location(lat, lon);
    // les couleurs
    PDLL = color(#0DBFBD, 100);
    PN = color(#D75F2A, 100);
    CYBERBASE = color(#483C00, 200);
    POINTCYB = color(#FF2D03, 200);
    ACCOORD = color(#2BCB17, 200);
    RENCONTRES = color(#D003FF, 100);
    IMMERSIONS = color(#4503FF, 100);
    CYBANJOU = color(#96AA00, 200);
  }

  void affiche(int prog) {
    ScreenPosition sp = map.getScreenPosition(ou);
    textSize(12);
    // affiche tous les EPN en PDLL
    if (prog == 0) {
      strokeWeight(7);
      stroke(PDLL);
      noFill();
      ellipse(sp.x, sp.y, taillePicto, taillePicto);
      noStroke();
      fill(PDLL);
      ellipse(sp.x, sp.y, taillePicto/4, taillePicto/4);

      if (overCircle(sp.x, sp.y, taillePicto)) {
        noStroke();
        fill(#FF9F03, 150);
        rect(mouseX, mouseY, 320, 100, 15);
        fill(0);
        textAlign(LEFT);
        text(nom, mouseX+10, mouseY+50);
        text(lieu, mouseX+10, mouseY + 60);
      }
    }
    // affiche les EPN inscrits sur PN
    if (prog == 1) {
      if (inscritPN == 1) {
        noFill();
        strokeWeight(8);
        stroke(PN);
        ellipse(sp.x, sp.y, taillePicto, taillePicto);
        if (overCircle(sp.x, sp.y, taillePicto)) {
          noStroke();
          fill(#FF9F03, 150);
          rect(mouseX, mouseY, 320, 100, 15);
          fill(0);                  
          textAlign(LEFT);
          text(nom, mouseX+10, mouseY+50);
          text(lieu, mouseX+10, mouseY + 60);
        }
      }
    }
    if (prog == 2) {
      if (label.toLowerCase().contains("cyber-base")) {
        noStroke();
        fill(CYBERBASE);
        ellipse(sp.x, sp.y, taillePicto/3, taillePicto/3);
      }
    }
    if (prog == 3) {
      if (label.toLowerCase().contains("point-cyb")) {
        noStroke();
        fill(POINTCYB);
        ellipse(sp.x, sp.y, taillePicto/3, taillePicto/3);
      }
    }
    if (prog == 4) {
      if (label.toLowerCase().contains("accoord") || label.toLowerCase().contains("emila")) {
        noStroke();
        fill(ACCOORD);
        ellipse(sp.x, sp.y, taillePicto/3, taillePicto/3);
      }
    }
    if (prog == 5) {
      if (label.toLowerCase().contains("cybanjou")) {
        fill(CYBANJOU);
        noStroke();        
        ellipse(sp.x, sp.y, taillePicto/3, taillePicto/3);
      }
    }
    if (prog == 6) {
      if (partenariat.toLowerCase().contains("immersion")) {
        fill(IMMERSIONS);
        noStroke();        
        ellipse(sp.x, sp.y, taillePicto, taillePicto);
      }
    }
    if (prog == 7) {
      if (partenariat.toLowerCase().contains("rencontres")) {
        fill(RENCONTRES);
        noStroke();        
        ellipse(sp.x, sp.y, taillePicto, taillePicto);
      }
    }
  }

  boolean overCircle(float x, float y, float diameter) {
    float disX = x - mouseX;
    float disY = y - mouseY;
    if (sqrt(sq(disX) + sq(disY)) < diameter/2 ) {
      return true;
    } 
    else {
      return false;
    }
  }
}

