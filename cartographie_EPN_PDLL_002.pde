/*
À faire : 
 réseau Fablab en PDLL
 réseau mission locale
 réseau relais emploi
 réseau lieux associatifs / collectivités
 réseau CSC
 réseau ECM
 Redéssiner le cadre en haut à gauche avec une sous partie "réseaux"
 outils : selection d'une zone avec la souris pour savoir combien il y a d'EPN dans cette zone.
 */

import de.fhpotsdam.unfolding.*;
import de.fhpotsdam.unfolding.geo.*;
import de.fhpotsdam.unfolding.utils.*;
import de.fhpotsdam.unfolding.utils.ScreenPosition;
import de.fhpotsdam.unfolding.marker.*;

UnfoldingMap map;
Location PdlL;

Table listeEPN;
ArrayList <LieuMediation> lesLieuxDeMediation;
int [] programme = {
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1
}; // en fonction de la valeur de programme l'affichage change
int [] chrono = {
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

int margeTitres = 20;
int HTitres = 25;

void setup() {
  size(1400, 1050); //SXGA+

  map = new UnfoldingMap(this);
  MapUtils.createDefaultEventDispatcher(this, map);
  PdlL = new Location(47.4f, -0.85f);
  map.zoomAndPanTo(PdlL, 9);

  lesLieuxDeMediation = new ArrayList<LieuMediation>();

  loadData();
  calculStatistiques();
}

void draw() {
  //background(255);
  map.draw();
  // cadre d'affichage en haut à gauche
  fill(255, 175);
  noStroke();
  rect(10, 5, 320, 240, 15); 
  // methode pour sélectionner à l'aide du clavier numérique le mode d'affichage voulu
  controle();
}


void loadData() {
  listeEPN = loadTable("tableau_EPN_région.csv");

  for (int i = 0 ; i < listeEPN.getRowCount(); i++) {
    lesLieuxDeMediation.add(new LieuMediation(
    listeEPN.getFloat(i, 1), 
    listeEPN.getFloat(i, 2), 
    listeEPN.getInt(i, 5), 
    listeEPN.getString(i, 0), 
    listeEPN.getString(i, 3), 
    listeEPN.getString(i, 6), 
    listeEPN.getString(i, 4), 
    listeEPN.getString(i, 7)));
  }
}
// quelques statistiques à développer, graphiquement surtout.
void calculStatistiques() {
  int NbEPNPDLL = 0;
  int NbEPNPN = 0;
  float pourcentageEPNinscritsPN = 0;
  for (int j = 0; j < lesLieuxDeMediation.size(); j++) {
    LieuMediation lm = lesLieuxDeMediation.get(j);
    if (lm.inscritPN == 1) {
      NbEPNPN++;
    }
    NbEPNPDLL++;
  }
  pourcentageEPNinscritsPN = (((float)NbEPNPN*100) / (float)NbEPNPDLL);
  println("Nombre d'EPN en PDLL = "+NbEPNPDLL+"  Nombre d'EPN inscrit sur PN = "+NbEPNPN+"  soit un pourcentage de = "+pourcentageEPNinscritsPN);
}

void controle() {
  // partie correspondant à l'interface de contrôle avec le clavier
  // pour sélectionner les différentes visualisations
  for (int i = 0; i < programme.length; i++) {
    // je converti i en char
    char temp = Integer.toString(i).charAt(0);
    if (keyPressed && key == temp) {
      if (chrono[i] == 0) {
        programme[i] = -programme[i];
        chrono[i]++;
      }
    }
    else chrono[i] = 0;

    if (programme[i] == 1) affiche(i);
  }
}

void affiche(int prog) {  

  switch (prog) {
    // on affiche tout les EPN de la région  
  case 0:
    for (int j = 0; j < lesLieuxDeMediation.size(); j++) {
      LieuMediation lm = lesLieuxDeMediation.get(j);
      lm.affiche(0);
    }
    textSize(22);
    fill(#0DBFBD, 200);
    text("EN PAYS DE LA LOIRE", margeTitres, HTitres);
    break;
    // on affiche les EPN inscrit sur PN
  case 1:
    for (int j = 0; j < lesLieuxDeMediation.size(); j++) {
      LieuMediation lm = lesLieuxDeMediation.get(j);
      lm.affiche(1);
    }
    textSize(22);
    fill(#D75F2A, 200);
    text("PARCOURS NUMÉRIQUES", margeTitres, 2*HTitres);
    break;
    // on affiche les cyber base
  case 2:
    for (int j = 0; j < lesLieuxDeMediation.size(); j++) {
      LieuMediation lm = lesLieuxDeMediation.get(j);
      lm.affiche(2);
    }
    textSize(22);
    fill(#483C00, 200);
    text("CYBERBASES", margeTitres, 3*HTitres);
    break;
    // on affiche les point cyb
  case 3:
    for (int j = 0; j < lesLieuxDeMediation.size(); j++) {
      LieuMediation lm = lesLieuxDeMediation.get(j);
      lm.affiche(3);
    }
    textSize(22);
    fill(#FF2D03, 200);
    text("POINT CYB", margeTitres, 4*HTitres);
    break;
    // on affiche les accoord et autres labels locaux
  case 4:
    for (int j = 0; j < lesLieuxDeMediation.size(); j++) {
      LieuMediation lm = lesLieuxDeMediation.get(j);
      lm.affiche(4);
    }
    textSize(22);
    fill(#2BCB17, 200);
    text("DIVERS (ACCOORD, EMIL@)", margeTitres, 5*HTitres);
    break;
    // on affiche les lieux du réseau Cybanjou
  case 5:
    for (int j = 0; j < lesLieuxDeMediation.size(); j++) {
      LieuMediation lm = lesLieuxDeMediation.get(j);
      lm.affiche(5);
    }
    textSize(22);
    fill(#96AA00, 200);
    text("CYBANJOU", margeTitres, 6*HTitres);
    break;
    // on affiche les lieux dans lesquels nous avons fait des immersions
  case 6:
    for (int j = 0; j < lesLieuxDeMediation.size(); j++) {
      LieuMediation lm = lesLieuxDeMediation.get(j);
      lm.affiche(6);
    }
    textSize(22);
    fill(#4503FF, 200);
    text("LES IMMERSIONS", margeTitres, 7*HTitres);
    break;
    // on affiche les lieux dans lesquels il y a eu des rencontres

  case 7:
    for (int j = 0; j < lesLieuxDeMediation.size(); j++) {
      LieuMediation lm = lesLieuxDeMediation.get(j);
      lm.affiche(7);
    }
    textSize(22);
    fill(#D003FF, 200);
    text("LES RENCONTRES", margeTitres, 8*HTitres);
    break;
  case 8:
    break;
  case 9:
    break;
  }
}

